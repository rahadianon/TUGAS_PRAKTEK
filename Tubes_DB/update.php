<!DOCTYPE html> 
	<html lang="en"> 
		<head> <meta charset="utf-8"> 
				<meta http-equiv="X-UA-Compatible" content="IE=edge">
				<meta name="viewport" content="width=device-width, initial-scale=1"> 
				<title>Salon System Management</title> 
				<link href="css/bootstrap.min.css" rel="stylesheet"> 
				<style type="text/css">
.jumbotron {
      background-color: #f4511e;
      color: #fff;
      padding: 100px 75px;
  }
.bg-grey {
      background-color: #f6f6f6;
  }
 .container-fluid {
      padding: 60px 50px;

</style>
		</head> 
	<body> 
	<div class="jumbotron text-center">
		<h3 align="center">SALON MANAGEMENT SYSTEM</h3>
		<legend></legend>
		<h3 align="center">Form Update Data</h3>
		</div>
		<table>
			<form action="update_act.php" method="POST">
			<tr>
				<td><label>Item ID </label></td>
				<td><input type="text" class="form-control" name="item_ID" placeholder="ID"></input></td>
			</tr>
			<tr>
				<td><label>Item Name </label></td>
				<td><input type="text" class="form-control" name="item_name" placeholder="Name"></input></td>
			</tr>
			<tr>
				<td><label>Expired Date </label></td>
				<td><input type="date" class="form-control" name="exp_date"></input></td>
			</tr>
			<tr>
				<td><label>Price </label></td>
				<td><input type="text" class="form-control" name="price" placeholder="$"></input></td>
			</tr>
			<tr>
				<td><label>Stock </label></td>
				<td><input type="text" class="form-control" name="stock" placeholder="Stock"></input></td>
			</tr>
			<tr>
			<tr>
				<td colspan="4" align="center">
				<button type="submit" class="btn btn-primary" name="update" value="Submit">Update</button></td>
			</tr>
			</tr>
		</form>
		</table>
	</body>
</html>