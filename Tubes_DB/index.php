<!DOCTYPE html>
<html lang="en">
<head>
  <title>Salon System Management</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
  .jumbotron {
      background-color: #f4511e;
      color: #fff;
      padding: 100px 25px;
  }
  .container-fluid {
      padding: 60px 50px;
  }
  .bg-grey {
      background-color: #f6f6f6;
  }
  .logo {
      font-size: 200px;
  }
  @media screen and (max-width: 768px) {
    .col-sm-4 {
      text-align: center;
      margin: 25px 0;
    }
  }
  </style>
</head>
<body>

<div class="jumbotron text-center">
  <h2>Salon System Management</h1> 
  <legend></legend>
  <p>Tugas Besar Basis Data</p> 
</div>

<!-- Container (About Section) -->
<div class="container-fluid" >
  <div class="row">
    <div class="col-sm-9">
        <div class="col-sm-4"><strong>Alvin Aditya Chandra</strong></div>
      <strong>3411151180 </strong>
      
    </div>
    <div class="col-sm-9">
        <div class="col-sm-4"><strong>Hendra Abdul Rochman</strong></div>
      <strong>3411151132 </strong>
      
    </div>
    <div class="col-sm-9">
        <div class="col-sm-4"><strong>Miftahul Falah</strong></div>
      <strong>3411151023 </strong>
      
    </div>
    <div class="col-sm-9">
        <div class="col-sm-4"></div>
      
    </div>
    <div class="col-sm-3">
      <span class="glyphicon glyphicon-scissors logo"></span>
      <i class="fa fa-scissors" aria-hidden="true"></i>
    </div>
    <div class="col-sm-9">
        <div class="col-sm-4"><strong>Deskripsi :</strong> :</div>
      
    </div>
    <div class="col-sm-9">
        <div class="col-sm-4">Aplikasi berbasis web ini di buat untuk melengkapi tugas akhir praktikum Basis Data</div>
      
    </div>
    
  </div>
</div>
<h6 align="center">IAM Credited Copyright © 2016</h6>
<!-- Container (Services Section) -->
<div class="container-fluid text-center bg-grey">
  <h2>Pager</h2>
  <h4>Page Index</h4>
  <br>
  <div class="row">
    <div class="col-sm-4">
     <a href="inputdata.php"> <span class="glyphicon glyphicon-plus-sign"></span></a>
      <h4>INPUT</h4>
      <p>Input New Item to Database</p>
    </div>
    <div class="col-sm-4">
      <a href="view.php"><span class="glyphicon glyphicon-minus-sign"></span></a>
      <h4>DELETE</h4>
      <p>Delete Item from Database</p>
    </div>
    <div class="col-sm-4">
      <span class="glyphicon glyphicon-search"></span>
      <h4>SEARCH</h4>
      <p>Looking for an Item from Database</p>
    </div>
  </div>
  <br><br>
  <div class="row">
    <div class="col-sm-4">
      
    </div>
    <div class="col-sm-4">
       <a href="viewupdate.php"><span class="glyphicon glyphicon-exclamation-sign"></span></a>
      <h4>UPDATE</h4>
      <p>Update Old to New Item</p>
    </div>
    <div class="col-sm-4">
      
    </div>
  </div>
</div>

</body>
</html>
